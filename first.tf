provider "aws" {
  profile = "default"
  region  = "us-east-2"
}

resource "aws_s3_bucket" "tf_course-one" {
  bucket  = "tf-course-20210804-one"
  acl     = "private"
}
